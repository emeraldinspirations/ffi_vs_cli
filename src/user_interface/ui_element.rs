#[derive(Clone)]
pub struct UiElement<'a> {
    pub short_name: &'a str,
    pub long_name: &'a str,
    pub short_help: &'a str,
    pub data_type: &'a str,
    pub default_value: Option<&'a str>,
    pub hint: &'a str,
    pub placeholder: &'a str,
    pub label: &'a str,
}
