use super::{ui::Ui, ui_element::UiElement};

lazy_static! {
    pub static ref INTEGER_GENERATOR_UI: Ui<'static> = Ui {
        title: "Integer Generator",
        short_description: "Pipes out a list of integers from <start> increasing by <step> \
            until stopped or memory overflow at 4294967294",
        long_description: "Example:
            integer_generator --start=2 --step=5 | head -n 3
            
            Outputs:
            2
            7
            12",
        element: vec![
            UiElement {
                short_name: "p",
                long_name: "step",
                short_help: "Overrides the default step value of 1",
                data_type: "u32",
                default_value: Some("1"),
                hint: "TBD",
                placeholder: "1",
                label: "Step",
            },
            UiElement {
                short_name: "s",
                long_name: "start",
                short_help: "Overrides the default starting value of 0",
                data_type: "usize",
                default_value: Some("1"),
                hint: "TBD",
                placeholder: "1",
                label: "Start",
            },
        ],
    };
}
