use super::{ui::Ui, ui_element::UiElement};

lazy_static! {
    pub static ref FIZZ_BUZZ_UI: Ui<'static> = Ui {
        title: "Fizz-Buzz",
        short_description: "An implementation of the Fizz-Buzz game.  When a number is piped \
        in it is replaced with:
        \"Fizz\" if the number is divisible by 3
        \"Buzz\" if the number is divisible by 5
        \"Fizz-Buzz\" if the number is divisible by both 3 and 5
        Otherwise the number is passed through.",
        long_description: "CRITERIA:
             <ModulousValue>:<NewValue>[;<CRITERIA>...]
             
             ModulousValue: An integer.  If the supplied value is divisible by\
             <ModulousValue> than <NewValue> is substituted.
             
             NewValue: The string substituted for the supplied value",
        element: vec![UiElement {
            short_name: "c",
            long_name: "criteria",
            short_help: "Overrides the default criteria",
            data_type: "Whatever",
            default_value: Some("{3:Fizz;5:Buzz;15:Fizz-Buzz}"),
            hint: "TBD",
            placeholder: "{3:Fizz;5:Buzz;15:Fizz-Buzz}",
            label: "Criteria",
        }],
    };
}
