#[derive(Clone)]
pub struct Ui<'a> {
    pub title: &'a str,
    pub short_description: &'a str,
    pub long_description: &'a str,
    // pub type_definition_vec: &'a str,
    pub element: Vec<super::ui_element::UiElement<'a>>,
}
