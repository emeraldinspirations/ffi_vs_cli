// ~/stdin_stdout_iterators.rs

use std::io::{self, BufRead, ErrorKind, Write};
use std::rc::Rc;

pub fn pipe_out(
    data: impl Iterator<Item = impl std::fmt::Display>,
) -> Result<bool, std::io::Error> {
    let stdout = io::stdout();
    let mut stdout_lock = stdout.lock();
    for datum in data {
        if let Err(e) = writeln!(stdout_lock, "{}", datum) {
            match e.kind() {
                ErrorKind::BrokenPipe => return Ok(false),
                _ => {
                    return Err(e);
                }
            }
        };
    }
    Ok(true)
}

pub struct PipeIn<'a> {
    stdin: *mut io::Stdin,
    stdin_lock: io::StdinLock<'a>,
    string_buffer: Rc<String>,
}

impl<'a> PipeIn<'a> {
    pub fn new(buffer_size: Option<usize>) -> Self {
        // Ignore the borrow checker by hiding Stdin behind a pointer
        let stdin = Box::into_raw(Box::new(io::stdin()));
        let string_buffer = Rc::new(match buffer_size {
            None => String::new(),
            Some(s) => String::with_capacity(s),
        });
        PipeIn {
            stdin,
            stdin_lock: unsafe { &*stdin }.lock(),
            string_buffer,
        }
    }
}

impl<'a> Drop for PipeIn<'a> {
    fn drop(&mut self) {
        // Turn Stdin back into a value so it gets dropped
        unsafe { Box::from_raw(self.stdin) };
    }
}

impl<'a> Iterator for PipeIn<'a> {
    type Item = Rc<String>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut data = Rc::make_mut(&mut self.string_buffer);
        data.clear();
        match self.stdin_lock.read_line(&mut data) {
            Ok(n) if n == 0 => None,
            Ok(_) => {
                data.truncate(data.trim_end().len());
                Some(self.string_buffer.clone())
            }
            Err(error) => {
                println!("error: {}", error);
                None
            }
        }
    }
}
