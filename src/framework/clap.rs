use super::super::user_interface::ui::Ui;
use clap::{App, Arg};

impl<'a> Ui<'a> {
    pub fn to_clap_app(&self) -> App {
        let mut app = App::new(self.title)
            .author(crate_authors!())
            .about(self.short_description)
            .version(crate_version!())
            .after_help(self.long_description);

        for element in self.element.iter() {
            app = app.arg(
                Arg::with_name(element.long_name)
                    .short(element.short_name)
                    .long(element.long_name)
                    .help(element.short_help)
                    .takes_value(element.data_type != "Flag"),
            );
        }

        app
    }
}
