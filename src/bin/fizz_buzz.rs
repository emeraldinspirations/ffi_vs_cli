// ~/src/bin/fizz_buzz.rs

use either::Either;
use ffi_vs_cli::fizz_buzz::{fizz_buzz, DEFAULT_CRITERIA};
use ffi_vs_cli::stdin_stdout_iterators::{pipe_out, PipeIn};
use ffi_vs_cli::user_interface::fizz_buzz_ui::FIZZ_BUZZ_UI;
use std::rc::Rc;

extern crate clap;

fn main() {
    process_command_line_arguments();
    pipe_out(
        PipeIn::new(Some(10)).map(|rc_string: Rc<String>| match rc_string.parse::<u32>() {
            Ok(x) => fizz_buzz(x, DEFAULT_CRITERIA),
            Err(_) => Either::Right("Error 1579094160: Unrecognized Piped Number"),
        }),
    )
    .unwrap();
}

fn process_command_line_arguments() {
    let ui = FIZZ_BUZZ_UI.clone();
    let matches = ui.to_clap_app().get_matches();

    let _criteria = matches
        .value_of("criteria")
        .unwrap_or("3:Fizz;5:Buzz;15:Fizz-Buzz");
}
