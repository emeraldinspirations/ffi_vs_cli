// ~/src/bin/head.rs

use ffi_vs_cli::stdin_stdout_iterators::{pipe_out, PipeIn};

use clap::{value_t, App, Arg};

fn main() {
    let matches = App::new("Head")
        .arg(Arg::with_name("lines").short("n").takes_value(true))
        .get_matches();
    let limit = value_t!(matches, "lines", usize).unwrap_or_else(|e| {
        if clap::ErrorKind::ArgumentNotFound == e.kind {
            10
        } else {
            e.exit()
        }
    });

    pipe_out(PipeIn::new(None).take(limit)).unwrap();
}
