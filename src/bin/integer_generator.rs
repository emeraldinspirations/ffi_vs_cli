// ~/integer_generator.rs

use ffi_vs_cli::integer_iterator;
use ffi_vs_cli::stdin_stdout_iterators::pipe_out;
use ffi_vs_cli::user_interface::integer_generator_ui::INTEGER_GENERATOR_UI;

extern crate clap;

fn main() -> std::io::Result<()> {
    let (start, step) = process_command_line_arguments();
    pipe_out(integer_iterator(start, step)).map(|_| ())
}

fn process_command_line_arguments() -> (u32, usize) {
    let ui = INTEGER_GENERATOR_UI.clone();
    let matches = ui.to_clap_app().get_matches();

    let start = matches
        .value_of("start")
        .unwrap_or("0")
        .parse::<u32>()
        .unwrap_or(0);

    let step = matches
        .value_of("step")
        .unwrap_or("1")
        .parse::<usize>()
        .unwrap_or(1);

    (start, step)
}
