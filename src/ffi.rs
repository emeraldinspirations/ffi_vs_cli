use crate::{fizz_buzz, integer_iterator};
use fizz_buzz::{fizz_buzz, DEFAULT_CRITERIA};
use std::ffi::CString;
use std::os::raw::c_char;
use std::panic::{catch_unwind, AssertUnwindSafe};
use std::ptr::NonNull;

#[no_mangle]
pub extern "C" fn fvc_foreach_int_as_fizzbuzz(
    cb: extern "C" fn(Option<NonNull<c_char>>) -> bool,
) -> u8 {
    catch_unwind(|| {
        for i in integer_iterator(0, 1) {
            let a = fizz_buzz(i, DEFAULT_CRITERIA);
            let cstring = NonNull::new(
                CString::new(a.either(|num| num.to_string(), |str_slice| str_slice.to_string()))
                    .ok()
                    .map_or_else(std::ptr::null_mut, |s| s.into_raw()),
            );
            let continue_ = cb(cstring);
            if let Some(cstring) = cstring {
                unsafe { CString::from_raw(cstring.as_ptr()) };
            }
            // https://api.jquery.com/jquery.each/
            if !continue_ {
                break;
            }
        }
    })
    .is_err() as u8
}

type OptionCString = Option<NonNull<c_char>>;
type IterInt = std::iter::StepBy<std::ops::Range<u32>>;
type BoxedIterString = Box<dyn Iterator<Item = OptionCString>>;

#[no_mangle]
pub extern "C" fn fvc_integer_iterator() -> Option<Box<IterInt>> {
    catch_unwind(|| Some(Box::new(integer_iterator(0, 1)))).ok()?
}

#[no_mangle]
pub extern "C" fn fvc_fizzbuzz(i: u32) -> Option<NonNull<c_char>> {
    NonNull::new(
        CString::new(
            fizz_buzz(i, DEFAULT_CRITERIA)
                .either(|num| num.to_string(), |str_slice| str_slice.to_string()),
        )
        .ok()
        .map_or_else(std::ptr::null_mut, |s| s.into_raw()),
    )
}

#[no_mangle]
pub extern "C" fn fvc_iter_map_int2cstring(
    boxed_iter: Option<Box<IterInt>>,
    func: extern "C" fn(u32) -> OptionCString,
) -> Option<Box<BoxedIterString>> {
    Some(Box::new(Box::new(
        // Clippy is wrong here because map expects FnMut
        #[allow(clippy::redundant_closure)]
        boxed_iter?.map(move |i| func(i)),
    )))
}

#[no_mangle]
pub extern "C" fn fvc_drop_boxed_iter_string(boxed_iter: Option<Box<BoxedIterString>>) -> u8 {
    // This is fine because if it fails, we shouldn't use it afterward anyway.
    catch_unwind(AssertUnwindSafe(|| {
        drop(boxed_iter);
    }))
    .is_err() as u8
}

#[no_mangle]
pub extern "C" fn fvc_iter_next_string(iter: Option<&mut BoxedIterString>) -> OptionCString {
    // This might not actually be unwind safe.
    // Has something to do with the reference to BoxedIterString.
    catch_unwind(AssertUnwindSafe(|| iter?.next()?)).ok()?
}

#[no_mangle]
pub extern "C" fn fvc_drop_cstring(cstring: Option<NonNull<c_char>>) -> u8 {
    catch_unwind(|| {
        // Intentionally panic on null pointer so this function returns an error
        unsafe { CString::from_raw(cstring.unwrap().as_ptr()) };
    })
    .is_err() as u8
}

#[test]
// This doesn't test that it gives the right output, just that it doesn't
// fail horribly... Check output with `cargo test test_ffi -- --nocapture`
fn test_ffi() -> Result<(), ()> {
    use std::ffi::CStr;

    fn panic_on_non_zero(err: u8) {
        if err != 0 {
            panic!("Got non-zero error code");
        }
    }

    println!("# FFI Callback:");
    // Use static variable to track iterations (can't use closure directly)
    static mut COUNT: u32 = 0;
    extern "C" fn my_cb(cstr_ptr: Option<NonNull<c_char>>) -> bool {
        if let Some(cstr_ptr) = cstr_ptr {
            println!(
                "{}",
                unsafe { CStr::from_ptr(cstr_ptr.as_ptr()) }
                    .to_str()
                    .expect("Invalid CString")
            );
        } else {
            panic!("cstr_ptr was None");
        }
        // Return false at 10 iterations
        unsafe {
            COUNT += 1;
            COUNT < 10
        }
    }
    panic_on_non_zero(fvc_foreach_int_as_fizzbuzz(my_cb));

    //vs

    println!("\n# Multiple FFI functions:");
    let mut boxed_iter = fvc_iter_map_int2cstring(fvc_integer_iterator(), fvc_fizzbuzz)
        // Eventually NoneError will stabilize and this will be unnecessary
        // https://doc.rust-lang.org/std/option/struct.NoneError.html
        .ok_or(())?;

    let iter = boxed_iter.as_mut();
    let mut count = 0;

    while let Some(cstr_ptr) = fvc_iter_next_string(Some(iter)) {
        // This function is pretending to be an FFI user who can't drop,
        // so we use CStr, which won't drop.
        println!(
            "{}",
            unsafe { CStr::from_ptr(cstr_ptr.as_ptr()) }
                .to_str()
                .unwrap_or("bad string")
        );
        // Now we call the drop function.
        panic_on_non_zero(fvc_drop_cstring(Some(cstr_ptr)));

        count += 1;
        if count >= 10 {
            break;
        }
    }

    panic_on_non_zero(fvc_drop_boxed_iter_string(Some(boxed_iter)));

    Ok(())
}
