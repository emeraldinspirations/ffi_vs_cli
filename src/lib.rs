pub mod ffi;
pub mod fizz_buzz;
pub mod framework;
pub mod stdin_stdout_iterators;
pub mod user_interface;

#[macro_use]
extern crate clap;

#[macro_use]
extern crate lazy_static;

pub fn integer_iterator(start: u32, step: usize) -> std::iter::StepBy<std::ops::Range<u32>> {
    (start..std::u32::MAX).step_by(step)
}
