use either::Either;

#[derive(Eq, PartialEq, Debug)]
pub struct Criterium<'a> {
    pub mod_val: u32,
    pub new_val: &'a str,
}

impl<'a> Criterium<'a> {
    pub fn from_string(matches: &str) -> Result<Vec<Criterium>, String> {
        matches
            .split(';')
            .map(|datum| {
                let mut split = datum.split(':');
                // Only match if we get exactly two elements from the iterator
                if let (Some(key), Some(value), None) = (split.next(), split.next(), split.next()) {
                    Ok(Criterium {
                        mod_val: key
                            .parse::<u32>()
                            .map_err(|_| format!("ParseIntError on \"{}\"", key))?,
                        new_val: value,
                    })
                } else {
                    Err(format!("Invalid Criterium source: {}", datum))
                }
            })
            .collect()
    }
}

pub const DEFAULT_CRITERIA: &[Criterium<'static>] = &[
    Criterium {
        mod_val: 3,
        new_val: "Fizz",
    },
    Criterium {
        mod_val: 5,
        new_val: "Buzz",
    },
    Criterium {
        mod_val: 15,
        new_val: "Fizz-Buzz",
    },
];

pub fn fizz_buzz<'a>(data: u32, criteria: &[Criterium<'a>]) -> Either<u32, &'a str> {
    criteria
        .iter()
        .filter(|criterium| (data % criterium.mod_val) == 0)
        .last()
        .map(|criterium| Either::Right(criterium.new_val))
        .unwrap_or(Either::Left(data))
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_fizz_buzz() {
        let criteria = vec![Criterium {
            mod_val: 12,
            new_val: "1578794665",
        }];
        assert_eq!(
            Some("1578794665"),
            fizz_buzz(12, &criteria).right(),
            "1578793622 - Fails if function undefined, returns wrong value"
        );
        assert_eq!(
            Some(1),
            fizz_buzz(1, &criteria).left(),
            "1578876912 - Fails if default value not returned"
        );
    }

    #[test]
    fn test_fizz_buzz_multiple_answers() {
        let criteria = vec![
            Criterium {
                mod_val: 3,
                new_val: "1578877124",
            },
            Criterium {
                mod_val: 5,
                new_val: "1578877154",
            },
        ];
        assert_eq!(
            Some("1578877154"),
            fizz_buzz(15, &criteria).right(),
            "1578877345 - Fails if last matching value not returned"
        );
    }

    #[test]
    fn test_criterium_from_string() {
        // zip short circuits, so this is done a bit more manually
        let iter_left = DEFAULT_CRITERIA.iter();
        let mut iter_right = Criterium::from_string("3:Fizz;5:Buzz;15:Fizz-Buzz")
            .unwrap()
            .into_iter();

        for left in iter_left {
            assert_eq!(Some(left), iter_right.next().as_ref());
        }

        // Make sure the right side is has nothing left
        assert_eq!(None, iter_right.next());
    }
}
