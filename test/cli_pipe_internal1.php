<?php
// Supposedly these can be made in order, but I had issues doing so.
$proc2=proc_open(
    __DIR__.'/../target/release/fizz_buzz',
    [
        0=>['pipe','r'],
        1=>['pipe','w'],
    ],
    $pipes2
);

if (!is_resource($proc2)) {exit("Failed to execute!\n");}

$proc1=proc_open(
    __DIR__.'/../target/release/integer_generator',
    [1=>$pipes2[0]],
    $pipes1
);

if (!is_resource($proc1)) {exit("Failed to execute!\n");}

while (true) {
    echo fgets($pipes2[1]);
}

// unreachable!();
proc_close($proc1);
proc_close($proc2);
