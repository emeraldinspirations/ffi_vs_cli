<?php
$proc=proc_open(
    __DIR__.'/../target/release/integer_generator | '.
    __DIR__.'/../target/release/fizz_buzz',
    [1=>['pipe','w']],
    $pipes
);

if (!is_resource($proc)) {exit("Failed to execute!\n");}

while (true) {
    echo fgets($pipes[1]);
}

// unreachable!();
proc_close($proc);
