<?php
declare(strict_types=1);
function fizz_buzz(int $i): string {
    $three=($i%3)===0;
    $five=($i%5)===0;
    if ($three&&$five) {return "Fizz-Buzz";}
    elseif ($three) {return "Fizz";}
    elseif ($five) {return "Buzz";}
    return "".$i;
}

for ($i=0; ; $i++) {
    echo fizz_buzz($i)."\n";
}
