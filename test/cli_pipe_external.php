<?php
// Use the command below to test this:
// php cli_pipe_external.php integer | php cli_pipe_external.php fizz_buzz | head -n 100
$err_text='Pass in "integer" or "fizz_buzz"'."\n";
if (!isset($argv[1])) {exit($err_text);}

if ($argv[1]==='integer') {
    $proc=proc_open(
        __DIR__.'/../target/release/integer_generator',
        [1=>['pipe','w']],
        $pipes
    );

    if (!is_resource($proc)) {exit("Failed to execute!\n");}

    while (true) {
        echo fgets($pipes[1]);
    }

    // unreachable!();
    proc_close($proc);
}
elseif ($argv[1]==='fizz_buzz') {
    $proc=proc_open(
        __DIR__.'/../target/release/fizz_buzz',
        [
            0=>STDIN,
            1=>['pipe','w'],
        ],
        $pipes
    );

    if (!is_resource($proc)) {exit("Failed to execute!\n");}

    while (true) {
        echo fgets($pipes[1]);
    }

    // unreachable!();
    proc_close($proc);
}
else {
    echo "\"{$argv[1]}\" is not a valid command\n", $err_text;
}
