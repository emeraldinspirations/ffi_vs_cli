# ffi_vs_cli

A project to determine if running a Rust executable from PHP via FFI is more
efficient than through CLI.

## Getting Started

These instructions will get you a copy of the project up and running on your
local machine for development and testing purposes. See deployment for notes on
how to deploy the project on a live system.

### Prerequisites

* Rust Compiler?

### Installing - TBD

```bash
cargo build --release
```

```bash
./bin/integer_generator u32 --start 1 --step 1 \
  | fizz_buzz --criteria="3:Fizz;5:Buzz;3&5:Fizz-Buzz;" \
  | head -n 50;
```

## Running the tests

```shell
cargo test
```

## Deployment - TBD

Add additional notes about how to deploy this on a live system

## Built With
Cargo?

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of
conduct, and the process for submitting pull requests to us.

## Versioning

We use [Semantic Versioning 2.0.0](http://semver.org/) for versioning. For the
versions available, see the [tags on this repository](https://gitlab.com/emeraldinspirations/ffi_vs_cli/-/tags).

## Authors

* **Damian Pound** - [chinoto](https://gitlab.com/chinoto)
* **Matthew "Juniper" Barlett** - [emeraldinspirations](https://gitlab.com/emeraldinspirations)

See also the list of [contributors](CONTRIBUTORS.md) who participated in this
project.

## License - TBD

This project is licensed under the MIT License - see the
[LICENSE.md](LICENSE)
file for details

## Acknowledgments - TBD

* Hat tip to anyone whose code was used
* Inspiration
* etc
